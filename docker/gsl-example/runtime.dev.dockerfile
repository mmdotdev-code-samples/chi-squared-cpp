
FROM ubuntu:focal-20210416

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Paris

RUN apt-get update -y && \
    apt-get install -y --no-install-recommends \
    libgsl23 \
    && apt-get -y autoremove \
    && apt-get -y clean

CMD ["/bin/bash"]

