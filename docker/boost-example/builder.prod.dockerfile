
FROM ubuntu:focal-20210416

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Paris

RUN apt-get update -y && \
    apt-get install -y --no-install-recommends \
    build-essential=12.8ubuntu1 \
    cmake=3.16.3-1ubuntu1\
    libboost-math-dev=1.71.0.0ubuntu2\
    && apt-get -y autoremove \
    && apt-get -y clean \
    && rm -rf /var/lib/apt/lists/*

CMD ["/bin/bash"]

