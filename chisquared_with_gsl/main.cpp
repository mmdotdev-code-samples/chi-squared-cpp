#include <gsl/gsl_cdf.h>
#include <iostream>

int main(int argc, char *argv[]) {

  const int kMaxNumDof = 100;
  const double kAlpha = 0.05;
  const double kHalfAlpha = kAlpha / 2.0;

  for (std::size_t n = 1; n <= kMaxNumDof; n++) {

    double lowerQ = gsl_cdf_chisq_Pinv(kHalfAlpha, n);
    double upperQ = gsl_cdf_chisq_Qinv(kHalfAlpha, n);

    // Show the two sided (1-alpha)% confidence region of the n (degrees of
    // freedom) chi-squared distribution
    std::cout << "[Chi^2_" << n << "(" << kHalfAlpha << "), Chi^2_" << n << "("
              << 1 - kHalfAlpha << ")]"
              << " = [" << lowerQ << ", " << upperQ << "]\n";
  }

  std::cout << std::flush;

  return 0;
}
