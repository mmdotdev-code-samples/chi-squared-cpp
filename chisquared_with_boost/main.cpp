#include <boost/math/distributions/chi_squared.hpp>
#include <iostream>

int main(int argc, char *argv[]) {

  const int kMaxNumDof = 100;
  const double kAlpha = 0.05;
  const double kHalfAlpha = kAlpha / 2.0;

  for (std::size_t n = 1; n <= kMaxNumDof; n++) {
    boost::math::chi_squared chiDist(n);
    double upperQ =
        (boost::math::quantile(boost::math::complement(chiDist, kHalfAlpha)));
    double lowerQ = (boost::math::quantile(chiDist, kHalfAlpha));

    // Show the two sided (1-alpha)% confidence region of the n (degrees of
    // freedom) chi-squared distribution
    std::cout << "[Chi^2_" << n << "(" << kHalfAlpha << "), Chi^2_" << n << "("
              << 1 - kHalfAlpha << ")]"
              << " = [" << lowerQ << ", " << upperQ << "]\n";
  }

  std::cout << std::flush;

  return 0;
}
