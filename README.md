# Chi-Squared in C++

This repository contains code to support the blog article on [blog.meanmachine.dev](https://blog.meanmachine.dev/post/2021-04-21-chi-squared).

Besides a simple code example, the repository also has the `.gitlab-ci.yml` file needed to create a CI pipeline. The pipeline is used to build and run the code samples in docker containers in order to showcase the claims made in the article.
